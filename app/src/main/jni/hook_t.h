
#define MAXSIZE 512


typedef struct hook_t{
    char clsname[MAXSIZE];
    char mname[MAXSIZE];
    char msig[MAXSIZE];
    jmethodID original;
    jclass hook_cls;
    jmethodID hook_meth;
    int type;
}hook_t;

// hook list
typedef struct hook_list {

}hook_list;