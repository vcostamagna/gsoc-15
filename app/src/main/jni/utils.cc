
#include "utils.h"



void *oldpointer;
/*
    @name checkAPIVersion
    @brief return the API version number
    @retval int API number version

*/
jint checkAPIVersion(JNIEnv *env){
    jclass versionClass = env->FindClass("android/os/Build$VERSION");
    jfieldID sdkIntFieldID = env->GetStaticFieldID(versionClass, "SDK_INT", "I");
    jint sdkInt = env->GetStaticIntField(versionClass, sdkIntFieldID);
    LOGI("API version: %d \n", sdkInt);
    return sdkInt;

}

int checkLollipop(JNIEnv *env)
{
    jint res = checkAPIVersion(env);
    if(res > 19 )
        return 1;
    else return 0;
}

unsigned int* search(unsigned int start, int gadget, int lollipop){
    LOGI("start: %x, gadget %x\n", start, gadget);
    unsigned int i = 0;
    unsigned int *pClazz;
    unsigned int *vtable;
    unsigned int *vmethods;
    //Get pointer to clazz and vtable for that class
    if(lollipop){
        pClazz = (unsigned int*) (start + CLAZZ_OFF);
        vtable =  (unsigned int*) ((*pClazz) +  VTABLE_OFF);
        vmethods =  (unsigned int*) ((*pClazz) +  LOLLIPOP_VMETHODS_OFF);
    }
    //differents offsets
    else{
        pClazz = (unsigned int*) (start + CLAZZ_OFF);
        vtable =  (unsigned int*) ((*pClazz) + VTABLE_OFF);
    }

    LOGI("clazz: 0x%08x, * = 0x%08x -> vtable=0x%08x, * = 0x%08x, vmethods = 0x%08x, * = 0x%08x \n", pClazz , *pClazz, vtable, *vtable, vmethods, *vmethods );;
    breakMe(4,4);
    /*
    unsigned int revgadget;
    revgadget =  ((gadget>>24)&0xff) | // move byte 3 to byte 0
                        ((gadget<<8)&0xff0000) | // move byte 1 to byte 2
                        ((gadget>>8)&0xff00) | // move byte 2 to byte 1
                        ((gadget<<24)&0xff000000); // byte 0 to byte 3
    LOGI("reverse : 0x%x\n ", revgadget);
    */
    unsigned char *g2 = (unsigned char*) &gadget; //this must be an uint*
    unsigned char *p = (unsigned char*)  *vtable; //this too
    //searching the method reference inside the class's vtable
    while(1){
        LOGI("check p 0x%08x and gadget 0x%08x \n",p, gadget);
        if(! memcmp(p, g2, 4 )){
            unsigned int* found_at = (unsigned int*) (*vtable + i) ;
            LOGI("target: 0x%08x at 0x%08x. vtable = %x, i = %d \n", gadget, found_at, *vtable, i);
            //changed = 1;

            return found_at;
        }
        p += 4; i += 4;
    }
    return 0;
}

jmethodID resolve_target(JNIEnv *env, jobject target, char * mname, char*  msig)
{
    jclass cls = env->GetObjectClass( target);
    return getMethodID(env, cls, mname, msig);
}

jmethodID getMethodID(JNIEnv *env,jclass cls, char *mname, char* msig){
    return env->GetMethodID(cls, mname, msig);
}


void analyze2(unsigned long targetm, int proxym, int lollipop)
{
    unsigned int *res = search( targetm, targetm, lollipop);
    if(res == 0) return;
    LOGI("trovato pointer at 0x%08x \n", res);
    LOGI("swappo 0x%08x con 0x%08x \n", targetm, proxym);
    swap(res, proxym);
    LOGI("analyze2 finito swap!!!! \n");
}

int analyze(JNIEnv *env, jobject c1, jobject c2, jobject thiz, int lollipop){
    LOGI("pointer to obj: %p e %p\n", c1, c2);
    int changed = 0;
    //get pointer to class object
    jclass cls1 = env->GetObjectClass( c1);
    jclass cls2 = env->GetObjectClass( c2);
    LOGI("pointer to classes: 0x%08x, %p e %p\n", &cls1, cls1, cls2);
    //get methodID
    jmethodID meth = getMethodID(env, cls1, MNAME, MSIG);
    jmethodID meth2 = getMethodID(env, cls2, MNAME, MSIG);
    if(!meth || !meth2) return changed;
    LOGI("meth c1 vale: 0x%08x, meth c2 vale: %p \n", meth, meth2);
    callTestJNI(env,c1); //helloprinter!
    unsigned int *res = search( (unsigned long) meth, (int) meth, lollipop);
    if(res == 0) return changed;
    else changed = 1;
    LOGI("trovato pointer at 0x%08x \n", res);
    //unsigned int *p = &(*res);
    //swap meth with meth2
    oldpointer = (void*) *res;;
    swap(res, (unsigned int) meth2);
    LOGI("oldpointer vale: 0x%0x \n", oldpointer);
    //check successful swap
    meth = getMethodID(env, cls1, MNAME, MSIG);
    meth2 = getMethodID(env, cls2, MNAME, MSIG);
    if(!meth || !meth2) return changed;
    LOGI("SECONDO GIRO: meth c1 vale: 0x%08x, meth c2 vale: 0x%0x \n", meth, meth2);
    breakMe(4,4);
    return changed;
}

void callTestJNI(JNIEnv *env, jobject obj){
  LOGI("chiamato testjni\n");
  jclass cls = env->GetObjectClass( obj);
  jmethodID mid = env->GetMethodID( cls, MNAME, MSIG);
  if (mid == 0)
    LOGI("error mid\n");
  jobject res = env->CallObjectMethod( obj, mid);
    const char *str = env->GetStringUTFChars((jstring) res, NULL);
    LOGI("ritornato: %s \n", str);
    breakMe(4,4);
}

int breakMe(int x, int y)
{
    LOGI(" BREAKME BREAKME!!! \n");
    int k = x * y;
    return k;
}

void swap(unsigned int *s, unsigned int d){
    *s = d;
}


void hookDevId(JNIEnv *env, jobject rawcls, int lollipop)
{

   // hook_t* res = create_hook(env,  char *clsname, HOOKM, HOOKMSIG, rawcls);
/*
    jclass cls = env->GetObjectClass( rawcls );
    jmethodID mid = env->GetMethodID( cls, "getDeviceId", "()Ljava/lang/String;");

    jclass hookcls = env->FindClass(HOOKCLS);
    jmethodID hookm = env->GetMethodID( hookcls, "getDeviceId", "()Ljava/lang/String;");
    analyze2((unsigned long) mid, (int) hookm, lollipop);
    */

}
char * get_dev_id(JNIEnv *env, jobject context)
{
    jclass cls = env->FindClass( "android/context/Context");
    // ---------------- cls == NULL all the time

    jmethodID mid = env->GetMethodID( cls, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jfieldID fid = env->GetStaticFieldID( cls, "TELEPHONY_SERVICE", "Ljava/lang/String;");
    jstring str = (jstring) env->GetStaticObjectField( cls, fid);
    jobject telephony = env->CallObjectMethod( context, mid, str);
    cls = env->FindClass( "android/telephony/TelephonyManager");
    mid =env->GetMethodID( cls, "getDeviceId", "()Ljava/lang/String;");
    str = (jstring) env->CallObjectMethod( telephony, mid);
    jsize len = env->GetStringUTFLength( str);
    char* deviceId = (char *) calloc(len + 1, 1);
    env->GetStringUTFRegion( str, 0, len, deviceId);
    env->DeleteLocalRef( str);
    /* to get a string in deviceId */

    return deviceId;
}
