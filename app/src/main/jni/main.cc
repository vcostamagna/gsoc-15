#include "test_sid_org_ndksample_MainActivity.h"
#include <sys/system_properties.h>

int changed = 0;
int lollipop = 0;

//entrypoint from Java
JNIEXPORT jstring JNICALL Java_test_sid_org_ndksample_MainActivity_callNativeCode
    (JNIEnv *env, jobject thiz, jobject cls1, jobject cls2)
{
    LOGI("chiamato con %p e %p \n", cls1,cls2);
    jint sdkInt = checkAPIVersion(env);
    if(sdkInt == 21){
        LOGI("LOLLIPOP DEVICE!!! \n");
        lollipop = 1;
    }
    if(!changed){
        int res = analyze(env,cls1,cls2, thiz, lollipop);
        if(res)
            changed = 1;
    }
    return env->NewStringUTF ( " Hello from JNI! " );
}
//entrypoint from Java
JNIEXPORT void JNICALL Java_test_sid_org_ndksample_MainActivity_callOriginalMethod
(JNIEnv *env, jobject callingObj)
{
    return;

}
/*
     entrypoint from Java
     @name callTestJNI
     @brief call java method using JNI
     @param thiz calling object
     @param obj target
     @retval void
*/
JNIEXPORT void JNICALL Java_test_sid_org_ndksample_MainActivity_callTestJNI
    (JNIEnv* env,jobject thiz, jobject obj)
{
    callTestJNI(env, obj);
}

JNIEXPORT void JNICALL Java_test_sid_org_ndksample_MainActivity_hookDevId
    (JNIEnv* env,jobject thiz, jobject obj)
{
    checkAPIVersion(env);
    hook_t* res = create_hook(env,  "ASD", HOOKM, HOOKMSIG, obj);
}







