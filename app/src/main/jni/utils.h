#include <android/log.h>
#include <jni.h>
#include <unistd.h>
#include <string.h>

#define TAG "NDKTEST"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__);


#define CLAZZ_OFF 0x8
#define VTABLE_OFF 0x34
#define LOLLIPOP_VTABLE_OFF 0x33 // 17*3
#define LOLLIPOP_VMETHODS_OFF 0x2f // 17*3
#define LOLLIPOP_CLAZZ_OFF 0x10 // 16

#define DEBUG 1
#define MNAME "getId"
#define MSIG "()Ljava/lang/String;"
#define CNAME "test/sid/org/ndksample/HelloPrinter"

#define HOOKCLS "test/sid/org/ndksample/HookCls"
#define HOOKM "getDeviceId"
#define HOOKMSIG "()Ljava/lang/String;"


jint checkAPIVersion(JNIEnv *env);
unsigned int* search(unsigned int start, int gadget, int );
int breakMe(int x, int y);
void swap(unsigned int *s, unsigned int d);
void hookDevId(JNIEnv *env, jobject rawcls, int );
char * get_dev_id(JNIEnv *env, jobject context);
void analyze2(unsigned long targetm, int proxym, int);
jmethodID getMethodID(JNIEnv *env,jclass cls, char *mname, char* msig);
int analyze(JNIEnv *env, jobject c1, jobject c2, jobject thiz, int);
void callTestJNI(JNIEnv *env, jobject obj);
jmethodID resolve_target(JNIEnv *env, jobject target, char *mname, char *msig);
int checkLollipop(JNIEnv *env);