#include "hook_helper.h"



static void getCallbackHook(JNIEnv *env, char* )
{

}

int set_hook(JNIEnv *env, hook_t *h)
{
    unsigned int *res = search( (unsigned int) h->original, (unsigned int) h->original, checkLollipop(env));
        if(res == 0) return 0;
    LOGI("trovato pointer at 0x%08x \n", res);
    LOGI("swappo 0x%08x con 0x%08x \n", h->original, h->hook_meth);
    swap(res, (unsigned int ) h->hook_meth);
    LOGI("analyze2 finito swap!!!! \n");
    return 1;
}

hook_t* create_hook(JNIEnv *env,  char *clsname, char* mname, char* msig, jobject target)
{
    hook_t* tmp = (hook_t*) malloc(sizeof(hook_t));
    strncpy(tmp->clsname, clsname, sizeof(tmp->clsname));
    strncpy(tmp->mname, mname, sizeof(tmp->mname));
    strncpy(tmp->msig, msig, sizeof(tmp->msig));

    jmethodID target_meth_ID = resolve_target(env, target, mname, msig);

    jclass hookcls = env->FindClass(HOOKCLS);
    jmethodID hookm = env->GetMethodID( hookcls, HOOKM, HOOKMSIG);

    memcpy(tmp->hook_cls, hookcls, 4);
    memcpy(tmp->hook_meth, hookm, 4);
    memcpy(tmp->original, target_meth_ID, 4);

    set_hook(env, tmp);

    return tmp;


}

int add_hook_to_struct(hook_t *h)
{

}

void get_hook()
{

}