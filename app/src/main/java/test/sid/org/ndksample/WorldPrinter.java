package test.sid.org.ndksample;

/**
 * Created by sid on 24/05/15.
 */
public class WorldPrinter extends CommonClass{
    public final String id = "WorldPrinter!";

    public String getId(){
        return id;
    }
    public void classMethod(){
        System.out.println("SONO IL CLASS METHOD DI " + this.getId());
    }
}
