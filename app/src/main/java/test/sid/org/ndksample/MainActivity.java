package test.sid.org.ndksample;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    public static HelloPrinter hp = new HelloPrinter();
    public static WorldPrinter wp = new WorldPrinter();

    static{
        System.loadLibrary("ndktest");
    }
    public native String callNativeCode(Object a, Object b);
    public native void callOriginalMethod();

    public native void callTestJNI(Object a);

    public native void hookDevId(Object a);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = (TextView) findViewById(R.id.my_textview);

        final Button button = (Button) findViewById(R.id.trigger_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                testCall();

                //tv.setText(s);
            }
        });
        final Button swapbutt = (Button) findViewById(R.id.swap_but);
        swapbutt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                nativeHelper();

                //tv.setText(s);
            }
        });
        final Button originalbutt = (Button) findViewById(R.id.original_but);
        originalbutt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                //callOriginalMethod();

                String s = getDeviceID();
                Log.d("NDKTEST", "JAVA1 RETURNED DEVICEID: " + s);
                TelephonyManager telephonyManager;

                telephonyManager =
                        (TelephonyManager) getSystemService(getApplicationContext().TELEPHONY_SERVICE);
                hookDevId(telephonyManager);

                s = getDeviceID();
                Log.d("NDKTEST", "JAVA2 RETURNED DEVICEID: " + s);
                //tv.setText(s);
            }
        });
        final Button jnibutt = (Button) findViewById(R.id.jni_but);
        jnibutt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                HelloPrinter newhp = new HelloPrinter();
                callTestJNI(newhp);

                //tv.setText(s);
            }
        });

    }
    public void nativeHelper(){
        testCall();
        callNativeCode(hp, wp);
        testCall();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public void testCall(){
        hp.print();
        wp.print();
    }

    public String getDeviceID() {
        TelephonyManager telephonyManager;

        telephonyManager =
                (TelephonyManager) getSystemService(getApplicationContext().TELEPHONY_SERVICE);


    /*
     * getDeviceId() function Returns the unique device ID.
     * for example,the IMEI for GSM and the MEID or ESN for CDMA phones.
     */
        String imeistring = telephonyManager.getDeviceId();
        return imeistring;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
