package test.sid.org.ndksample;

/**
 * Created by sid on 24/05/15.
 */
public class HelloPrinter extends CommonClass {
    public final String id = "HelloPrinter!";

    public String getId(){
        return id;
    }
    public void classMethodH(){
        System.out.println("SONO IL CLASS METHOD DI " + this.getId());
    }
}
